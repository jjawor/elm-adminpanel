package com.jjawor.elm;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.jjawor.elm.schemas.Answer;
import com.jjawor.elm.schemas.AssignTestToCourseRequest;
import com.jjawor.elm.schemas.AssignTestToCourseResponse;
import com.jjawor.elm.schemas.AssignUserToCourseRequest;
import com.jjawor.elm.schemas.AssignUserToCourseResponse;
import com.jjawor.elm.schemas.Category;
import com.jjawor.elm.schemas.Course;
import com.jjawor.elm.schemas.CreateAnswerRequest;
import com.jjawor.elm.schemas.CreateAnswerResponse;
import com.jjawor.elm.schemas.CreateCategoryRequest;
import com.jjawor.elm.schemas.CreateCategoryResponse;
import com.jjawor.elm.schemas.CreateCourseRequest;
import com.jjawor.elm.schemas.CreateCourseResponse;
import com.jjawor.elm.schemas.CreateQuestionRequest;
import com.jjawor.elm.schemas.CreateQuestionResponse;
import com.jjawor.elm.schemas.CreateTestRequest;
import com.jjawor.elm.schemas.CreateTestResponse;
import com.jjawor.elm.schemas.CreateUserRequest;
import com.jjawor.elm.schemas.CreateUserResponse;
import com.jjawor.elm.schemas.GetCategoryQuestionListRequest;
import com.jjawor.elm.schemas.GetCategoryQuestionListResponse;
import com.jjawor.elm.schemas.GetCourseTestListRequest;
import com.jjawor.elm.schemas.GetCourseTestListResponse;
import com.jjawor.elm.schemas.GetCourseUserListRequest;
import com.jjawor.elm.schemas.GetCourseUserListResponse;
import com.jjawor.elm.schemas.GetStudentResultsListRequest;
import com.jjawor.elm.schemas.GetStudentResultsListResponse;
import com.jjawor.elm.schemas.GetUserListRequest;
import com.jjawor.elm.schemas.GetUserListResponse;
import com.jjawor.elm.schemas.Question;
import com.jjawor.elm.schemas.RequestType;
import com.jjawor.elm.schemas.ResultStatus;
import com.jjawor.elm.schemas.Test;
import com.jjawor.elm.schemas.TestResult;
import com.jjawor.elm.schemas.User;
import com.jjawor.elm.ws.WSCommons;

@Service("courseService")
public class CourseService{
	@Autowired
	private  WebServiceTemplate getCourseUserListWS;
	@Autowired
	private  WebServiceTemplate getCourseTestListWS;
	@Autowired
	private  WebServiceTemplate getCategoryQuestionListWS;
	@Autowired
	private  WebServiceTemplate createCourseWS;
	@Autowired
	private  WebServiceTemplate createUserWS;
	@Autowired
	private  WebServiceTemplate createCategoryWS;
	@Autowired
	private  WebServiceTemplate createQuestionWS;
	@Autowired
	private  WebServiceTemplate createAnswerWS;
	@Autowired
	private  WebServiceTemplate assignUserToCourseWS;
	@Autowired
	private  WebServiceTemplate elmWS;
		public List<Category> getCategoryQuestions(SessionData sessionData){
			GetCategoryQuestionListRequest request=new GetCategoryQuestionListRequest();
			request.setToken(sessionData.getWSToken());
			GetCategoryQuestionListResponse res=(GetCategoryQuestionListResponse) getCategoryQuestionListWS.marshalSendAndReceive(request);
			if(!WSCommons.handeWSResponseErrors(res))return null;
			return res.getCategories();
		}
		public List<Course> getCourseUsers(SessionData sessionData){
			 GetCourseUserListRequest request=new GetCourseUserListRequest();
			 request.setToken(sessionData.getWSToken());
			 GetCourseUserListResponse res=(GetCourseUserListResponse) getCourseUserListWS.marshalSendAndReceive(request);
			 if(!WSCommons.handeWSResponseErrors(res))return null;
			 return res.getCourses();
		}
		public List<Course> getCourseTests(SessionData sessionData){
			 GetCourseTestListRequest request=new GetCourseTestListRequest();
			 request.setToken(sessionData.getWSToken());
			 GetCourseTestListResponse res=(GetCourseTestListResponse) getCourseTestListWS.marshalSendAndReceive(request);
			 if(!WSCommons.handeWSResponseErrors(res))return null;
			 return res.getCourses();
		}
		public List<TestResult> getStudentResults(Test test,Course course,SessionData sessionData){
			GetStudentResultsListRequest request=new GetStudentResultsListRequest();
			 request.setToken(sessionData.getWSToken());
			 request.setCourseId(test.getId());
			 request.setTestId(test.getId());
			 GetStudentResultsListResponse res=(GetStudentResultsListResponse) elmWS.marshalSendAndReceive(request);
			 if(!WSCommons.handeWSResponseErrors(res))return null;
			 return res.getResults();
		}
		public long persistCourse(SessionData sessionData,Course course){
			CreateCourseRequest request=new CreateCourseRequest();
			request.setToken(sessionData.getWSToken());
			request.setCourse(course);
			if(course.getId()==null){
				request.setOperationType(RequestType.CREATE);
			}
			else
			{
				request.setOperationType(RequestType.MODIFY);
			}
			CreateCourseResponse res=(CreateCourseResponse) createUserWS.marshalSendAndReceive(request);
			if(!WSCommons.handeWSResponseErrors(res))return -1l;
			return res.getId().longValue();
		}
		public long persistCategory(SessionData sessionData,Category cat){
			CreateCategoryRequest request=new CreateCategoryRequest();
			request.setToken(sessionData.getWSToken());
			request.setCategory(cat);
			if(cat.getId()==null){
				request.setOperationType(RequestType.CREATE);
			}
			else
			{
				request.setOperationType(RequestType.MODIFY);
			}
			CreateCategoryResponse res=(CreateCategoryResponse) createUserWS.marshalSendAndReceive(request);
			if(!WSCommons.handeWSResponseErrors(res))return -1l;
			return res.getId().longValue();
		}
		public long persistQuestion(SessionData sessionData,Question cat){
			CreateQuestionRequest request=new CreateQuestionRequest();
			request.setToken(sessionData.getWSToken());
			request.setQuestion(cat);
			if(cat.getId()==null){
				request.setOperationType(RequestType.CREATE);
			}
			else
			{
				request.setOperationType(RequestType.MODIFY);
			}
			CreateQuestionResponse res=(CreateQuestionResponse) createQuestionWS.marshalSendAndReceive(request);
			if(!WSCommons.handeWSResponseErrors(res))return -1l;
			return res.getId().longValue();
		}
		public long persistAnswer(SessionData sessionData,Answer cat){
			CreateAnswerRequest request=new CreateAnswerRequest();
			request.setToken(sessionData.getWSToken());
			request.setAnswer(cat);
			if(cat.getId()==null){
				request.setOperationType(RequestType.CREATE);
			}
			else
			{
				request.setOperationType(RequestType.MODIFY);
			}
			CreateAnswerResponse res=(CreateAnswerResponse) createAnswerWS.marshalSendAndReceive(request);
			if(!WSCommons.handeWSResponseErrors(res))return -1l;
			return res.getId().longValue();
		}
		public long persistTest(SessionData sessionData,Test cat){
			CreateTestRequest request=new CreateTestRequest();
			request.setToken(sessionData.getWSToken());
			request.setTest(cat);
			if(cat.getId()==null){
				request.setOperationType(RequestType.CREATE);
			}
			else
			{
				request.setOperationType(RequestType.MODIFY);
			}
			CreateTestResponse res=(CreateTestResponse) elmWS.marshalSendAndReceive(request);
			if(!WSCommons.handeWSResponseErrors(res))return -1l;
			return res.getId().longValue();
		}
		public long assignTestToCourse(SessionData sessionData,Test cat,Course course){
			AssignTestToCourseRequest request=new AssignTestToCourseRequest();
			request.setToken(sessionData.getWSToken());
			request.setTestID(cat.getId());
			request.setCourseID(course.getId());
			request.setOperationType(RequestType.CREATE);
			AssignTestToCourseResponse res=(AssignTestToCourseResponse) elmWS.marshalSendAndReceive(request);
			if(!WSCommons.handeWSResponseErrors(res))return -1l;
			return res.getId().longValue();
		}
		public boolean deleteCourse(SessionData sessionData,Course course){
			CreateCourseRequest request=new CreateCourseRequest();
			request.setToken(sessionData.getWSToken());
			request.setCourse(course);
			request.setOperationType(RequestType.DELETE);
			CreateCourseResponse res=(CreateCourseResponse) createUserWS.marshalSendAndReceive(request);
			if(!WSCommons.handeWSResponseErrors(res))return false;
			return true;
		}
		public boolean deleteCategory(SessionData sessionData,Category cat){
			CreateCategoryRequest request=new CreateCategoryRequest();
			request.setToken(sessionData.getWSToken());
			request.setCategory(cat);
			request.setOperationType(RequestType.DELETE);
			CreateCategoryResponse res=(CreateCategoryResponse) createCategoryWS.marshalSendAndReceive(request);
			if(!WSCommons.handeWSResponseErrors(res))return false;
			return true;
		}
		public boolean deleteQuestion(SessionData sessionData,Question cat){
			CreateQuestionRequest request=new CreateQuestionRequest();
			request.setToken(sessionData.getWSToken());
			request.setQuestion(cat);
			request.setOperationType(RequestType.DELETE);
			CreateQuestionResponse res=(CreateQuestionResponse) createQuestionWS.marshalSendAndReceive(request);
			if(!WSCommons.handeWSResponseErrors(res))return false;
			return true;
		}
		public boolean deleteAnswer(SessionData sessionData,Answer cat){
			CreateAnswerRequest request=new CreateAnswerRequest();
			request.setToken(sessionData.getWSToken());
			request.setAnswer(cat);
			request.setOperationType(RequestType.DELETE);
			CreateAnswerResponse res=(CreateAnswerResponse) elmWS.marshalSendAndReceive(request);
			if(!WSCommons.handeWSResponseErrors(res))return false;
			return true;
		}
		public boolean deleteTest(SessionData sessionData,Test cat){
			CreateTestRequest request=new CreateTestRequest();
			request.setToken(sessionData.getWSToken());
			request.setTest(cat);
			request.setOperationType(RequestType.DELETE);
			CreateTestResponse res=(CreateTestResponse) elmWS.marshalSendAndReceive(request);
			if(!WSCommons.handeWSResponseErrors(res))return false;
			return true;
		}
		public long assign(SessionData sessionData, Course course, User user) {
			AssignUserToCourseRequest request=new AssignUserToCourseRequest();
			request.setToken(sessionData.getWSToken());
			request.setCourseID(course.getId());
			request.setUserID(user.getId());
			request.setOperationType(RequestType.CREATE);
			AssignUserToCourseResponse res=(AssignUserToCourseResponse) assignUserToCourseWS.marshalSendAndReceive(request);
			if(!WSCommons.handeWSResponseErrors(res))return -1l;
			return res.getId().longValue();
		}
		public boolean unAssign(SessionData sessionData, Course course, User user) {
			AssignUserToCourseRequest request=new AssignUserToCourseRequest();
			request.setToken(sessionData.getWSToken());
			request.setCourseID(course.getId());
			request.setUserID(user.getId());
			request.setOperationType(RequestType.DELETE);
			AssignUserToCourseResponse res=(AssignUserToCourseResponse) assignUserToCourseWS.marshalSendAndReceive(request);
			if(!WSCommons.handeWSResponseErrors(res))return false;
			return true;
		}
}
