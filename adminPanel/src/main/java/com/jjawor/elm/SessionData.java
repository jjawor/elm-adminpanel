package com.jjawor.elm;

import java.io.Serializable;
import java.math.BigInteger;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

//@Component
//@Scope(value="session")
public class SessionData implements Serializable{
	private static final long serialVersionUID = 1L;
	private String wSToken;
	private BigInteger userId;
	public String getWSToken() {
		return wSToken;
	}

	public void setWSToken(String wSToken) {
		this.wSToken = wSToken;
	}

	public BigInteger getUserId() {
		return userId;
	}

	public void setUserId(BigInteger userId) {
		this.userId = userId;
	}

}
