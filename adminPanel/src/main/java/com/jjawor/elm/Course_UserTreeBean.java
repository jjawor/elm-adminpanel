package com.jjawor.elm;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;

import org.primefaces.model.TreeNode;  
import org.primefaces.model.DefaultTreeNode;  
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.jjawor.elm.schemas.Course;
import com.jjawor.elm.schemas.User;
  

@Component("course_userTreeBean")
@Scope(value="session")
public class Course_UserTreeBean implements Serializable{  
	private static final long serialVersionUID = 1L;
	@Autowired
	private transient CourseService courseService;   
	@Autowired
	private transient MessagesController messagesController;//for messagess
	@Autowired
	private SessionData sessionData;
	@Autowired
	private StudentsBean studentsBean;
    private TreeNode root;  
    
    private final String rootType="kroot";
	private final String courseType="course";
    private final String userType="user";

	private List<User> students;
	private String newCourse;
	private User newStudent;
    private TreeNode selectedNode;
    private boolean result;
    public boolean isLastSuccess(){
    	return result;
    }

    public List<User> getStudents() {
		return students;
	}

	public void setStudents(List<User> students) {
		this.students = students;
	}
    
	@PostConstruct
    public void init() {  
        root = new DefaultTreeNode("Root", null); 
        students=studentsBean.getStudents();
        TreeNode main = new DefaultTreeNode(rootType,"Kursy", root);
        main.setExpanded(true);
        for(Course course:courseService.getCourseUsers(sessionData))
        {
        	TreeNode courseNode = new DefaultTreeNode(courseType,new CourseTreeWrapper(course), main); 
        	for(BigInteger bi:course.getStudentsIDs()){
        		User user=new User();user.setId(bi);
        		User realUser=students.get(Collections.binarySearch(students, user, new UserIdComparator()));
        		new DefaultTreeNode(userType,new UserTreeWrapper(realUser), courseNode);
        	}
        		
        }
    }  
	public void persist(ActionEvent actionEvent){
		Course course=new Course();
		course.setName(getNewCourse());
		result=courseService.persistCourse(sessionData, course)<0?false:true;
		if(result)messagesController.addOperationSuccess(actionEvent);
		else messagesController.addOperationError(actionEvent);
	}
	public void assign(ActionEvent actionEvent){
		Object nodeData=getSelectedNode().getData();
		System.out.println(nodeData);
		if(nodeData instanceof CourseTreeWrapper){
			Course course=((CourseTreeWrapper) nodeData).getCourse();
			User user=null;
			for(User s:students)
				if(s.getLogin().equals(this.getNewStudent().getLogin()))
					{user=s;break;}
			System.out.println(user+" "+course);
			if(user==null)result=false;
			else result=courseService.assign(sessionData, course,user)<0?false:true;
			}
		if(result)messagesController.addOperationSuccess(actionEvent);
		else messagesController.addOperationError(actionEvent);
	}
	public void delete(ActionEvent actionEvent){
		Object nodeData=getSelectedNode().getData();
		if(nodeData instanceof CourseTreeWrapper){
		Course course=((CourseTreeWrapper) nodeData).getCourse();
		result=courseService.deleteCourse(sessionData, course);
		}
		else if(nodeData instanceof UserTreeWrapper){
			User user=((UserTreeWrapper) nodeData).getUser();
			Object parentData=getSelectedNode().getParent().getData();
			if(parentData instanceof CourseTreeWrapper){
			result=courseService.unAssign(sessionData, ((CourseTreeWrapper) parentData).getCourse(),user);
			}
			else result=false;
			}
		else result=false;
		if(result)messagesController.addOperationSuccess(actionEvent);
		else messagesController.addOperationError(actionEvent);
	}
	/*public boolean persist(String courseName){
		Course course=new Course();
		course.setName(courseName);
		return (courseService.persistCourse(sessionData, course)<0?false:true);
	}
	public boolean delete(TreeNode node){
		//Course course=new Course();
		//course.setName(courseName);
		return true;//(courseService.persistCourse(sessionData, course)<0?false:true);
	}*/
	public Course_UserTreeBean getRefreshed(){
		init();
		return this;
	}
	
    public TreeNode getRoot() {  
        return root;  
    }
    
	public TreeNode getSelectedNode() {
		return selectedNode;
	}
	public void setSelectedNode(TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}
	public String getNewCourse() {
		return newCourse;
	}
	public void setNewCourse(String newCourse) {
		this.newCourse = newCourse;
	}

	public User getNewStudent() {
		if(newStudent==null)newStudent=new User();
		return newStudent;
	}

	public void setNewStudent(User newStudent) {
		this.newStudent = newStudent;
	}

	public String getRootType() {
		return rootType;
	}

	public String getCourseType() {
		return courseType;
	}

	public String getUserType() {
		return userType;
	}
	
}
