package com.jjawor.elm;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.xml.crypto.Data;

import org.primefaces.model.TreeNode;  
import org.primefaces.model.DefaultTreeNode;  
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.jjawor.elm.schemas.Category;
import com.jjawor.elm.schemas.Course;
import com.jjawor.elm.schemas.Punctation;
import com.jjawor.elm.schemas.Test;
import com.jjawor.elm.schemas.User;
  

@Component("course_testTreeBean")
@Scope(value="session")
public class Course_TestTreeBean implements Serializable{  
	private static final long serialVersionUID = 1L;
	@Autowired
	private transient CourseService courseService;   
	@Autowired
	private transient MessagesController messagesController;//for messagess
	@Autowired
	private SessionData sessionData;
	@Autowired
	private Category_QuestionTreeBean category_QuestionTreeBean;
   
	
	
	//tree data
	private TreeNode root; 
    private final String rootType="kroot";
	private final String courseType="course";
    private final String testType="test";
    //category mini tree data
    private TreeNode categoriesRoot;
    private TreeNode [] categoriesSelectedNodes;

    //selection and creating
	private String newCourse;
	private Test newTest;
    private TreeNode selectedNode;
    private Test selectedTest;
    private Course selectedCourse;
    private boolean result;
    
    
    public boolean isLastSuccess(){
    	return result;
    }

   public void translateSelected(){
	   Object selectedTest=this.getSelectedNode().getData();
	   Object selectedCourse=this.getSelectedNode().getParent().getData();
	   if(selectedTest instanceof TestTreeWrapper && selectedCourse instanceof CourseTreeWrapper){
		   this.setSelectedTest(((TestTreeWrapper) selectedTest).getTest());
		   this.setSelectedCourse(((CourseTreeWrapper) selectedCourse).getCourse());
	   }
   }
    
	@PostConstruct
    public void init() {  
        root = new DefaultTreeNode("Root", null); 
        TreeNode main = new DefaultTreeNode(rootType,"Kursy", root);
        main.setExpanded(true);
        for(Course course:courseService.getCourseTests(sessionData))
        {
        	TreeNode courseNode = new DefaultTreeNode(courseType,new CourseTreeWrapper(course), main); 
        	for(Test test:course.getTests()){
        		new DefaultTreeNode(testType,new TestTreeWrapper(test), courseNode);
        	}
        		
        }
        categoriesRoot=category_QuestionTreeBean.getCategoriesRoot();
    }  
	public void persistCourse(ActionEvent actionEvent){
		Course course=new Course();
		course.setName(getNewCourse());
		result=courseService.persistCourse(sessionData, course)<0?false:true;
		if(result)messagesController.addOperationSuccess(actionEvent);
		else messagesController.addOperationError(actionEvent);
	}
	public void persistTest(ActionEvent actionEvent){
		List<BigInteger> categories=getNewTest().getCategoriesIDs();categories.clear();
		for(TreeNode node:categoriesSelectedNodes){
			Object nodeData=node.getData();
			if(nodeData instanceof CategoryTreeWrapper){
				categories.add(((CategoryTreeWrapper) nodeData).getCategory().getId());
			}
		}
		Object course=selectedNode.getData();
		if(course instanceof CourseTreeWrapper){
		long newId;
		result=(newId=courseService.persistTest(sessionData, getNewTest()))<0?false:true;
		getNewTest().setId(BigInteger.valueOf(newId));
		if(result)result=courseService.assignTestToCourse(sessionData, getNewTest(),((CourseTreeWrapper)course).getCourse())<0?false:true;
		else courseService.deleteTest(sessionData, getNewTest());//rollback if false
		newTest=null;
		}
		else if(getNewTest().getId()!=null){
			result=courseService.persistTest(sessionData, getNewTest())<0?false:true;
		}
		else{result=false;return;}
		
		if(result)messagesController.addOperationSuccess(actionEvent);
		else messagesController.addOperationError(actionEvent);
	}
	/*public void delete(ActionEvent actionEvent){
		Object nodeData=getSelectedNode().getData();
		if(nodeData instanceof CourseTreeWrapper){
		Course course=((CourseTreeWrapper) nodeData).getCourse();
		result=courseService.deleteCourse(sessionData, course);
		}
		else if(nodeData instanceof UserTreeWrapper){
			User user=((UserTreeWrapper) nodeData).getUser();
			Object parentData=getSelectedNode().getParent().getData();
			if(parentData instanceof CourseTreeWrapper){
			result=courseService.unAssign(sessionData, ((CourseTreeWrapper) parentData).getCourse(),user);
			}
			else result=false;
			}
		else result=false;
		if(result)messagesController.addOperationSuccess(actionEvent);
		else messagesController.addOperationError(actionEvent);
	}*/
	public void deleteCourse(ActionEvent actionEvent){
		Object nodeData=getSelectedNode().getData();
		if(nodeData instanceof CourseTreeWrapper){
		Course course=((CourseTreeWrapper) nodeData).getCourse();
		result=courseService.deleteCourse(sessionData, course);
		}
		else result=false;
		if(result)messagesController.addOperationSuccess(actionEvent);
		else messagesController.addOperationError(actionEvent);
	}
	public void deleteTest(ActionEvent actionEvent){
		Object nodeData=getSelectedNode().getData();
		if(nodeData instanceof TestTreeWrapper){
			Test test=((TestTreeWrapper) nodeData).getTest();
			result=courseService.deleteTest(sessionData, test);
		}
		else result=false;
		if(result)messagesController.addOperationSuccess(actionEvent);
		else messagesController.addOperationError(actionEvent);
	}
	public void resetNewTest(ActionEvent actionEvent)
	{
		this.newTest=null;
	}
	public void setSelectedTestAsNew(ActionEvent actionEvent)
	{
		Object data=this.getSelectedNode().getData();
		if(data instanceof TestTreeWrapper){
			this.newTest=((TestTreeWrapper) data).getTest();
			List<BigInteger> toBeSelected=newTest.getCategoriesIDs();
			for(TreeNode node:categoriesRoot.getChildren())
				{
				Object catO=node.getData();
				if(catO instanceof CategoryTreeWrapper){
					Category cat=((CategoryTreeWrapper) catO).getCategory();
					if(toBeSelected.contains(cat.getId()))
						node.setSelected(true);
				}
				}
		}	
	}
	public Course_TestTreeBean getRefreshed(){
		init();
		return this;
	}
	
    public TreeNode getRoot() {  
        return root;  
    }
    
	public TreeNode getSelectedNode() {
		return selectedNode;
	}
	public void setSelectedNode(TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}
	public String getNewCourse() {
		return newCourse;
	}
	public void setNewCourse(String newCourse) {
		this.newCourse = newCourse;
	}

	public String getRootType() {
		return rootType;
	}

	public String getCourseType() {
		return courseType;
	}

	public String getTestType() {
		return testType;
	}



	public Test getNewTest() {
		if(newTest==null){newTest=new Test();newTest.setPunctation(new Punctation());}
		return newTest;
	}


	public void setNewTest(Test newTest) {
		this.newTest = newTest;
	}



	public TreeNode getCategoriesRoot() {
		return categoriesRoot;
	}



	public void setCategoriesRoot(TreeNode categoriesRoot) {
		this.categoriesRoot = categoriesRoot;
	}
	
	public String getCategoryType() {
		return category_QuestionTreeBean.getCategoryType();
	}



	public TreeNode [] getCategoriesSelectedNodes() {
		return categoriesSelectedNodes;
	}



	public void setCategoriesSelectedNodes(TreeNode [] categoriesSelectedNodes) {
		this.categoriesSelectedNodes = categoriesSelectedNodes;
	}



	public Test getSelectedTest() {
		return selectedTest;
	}



	public void setSelectedTest(Test selectedTest) {
		this.selectedTest = selectedTest;
	}



	public Course getSelectedCourse() {
		return selectedCourse;
	}



	public void setSelectedCourse(Course selectedCourse) {
		this.selectedCourse = selectedCourse;
	}
}
