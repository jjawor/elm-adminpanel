/**
 *
 */
package com.jjawor.elm.professor;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jjawor.elm.CourseService;
import com.jjawor.elm.SessionData;
import com.jjawor.elm.StudentsBean;
import com.jjawor.elm.commons.Formats;
import com.jjawor.elm.schemas.Course;
import com.jjawor.elm.schemas.Test;
import com.jjawor.elm.schemas.TestResult;
@Service("resultsModel")
@Scope(value = "session", proxyMode = org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS) 
public class StudentResultsDataModel extends LazyDataModel<TestResult> {
	private static final long serialVersionUID = -8832831134966938627L;
	@Autowired
	private SessionData sessionData;
	@Autowired
	private StudentsBean studentsBean;
	@Autowired
	private CourseService courseService;
	private List<TestResult> results;
	private List<TestResult> cachedResults;
	//test and course -> for refreash
	Test test;
	Course course;
	
    public void init(Test test,Course course){
    	this.test=test;
    	this.course=course;
		results=courseService.getStudentResults(test, course, sessionData);
		for(TestResult tr:results)
			tr.setUser(studentsBean.getStudentById(tr.getUserId()));
	 }

	@Override
	public List<TestResult> load(int first, int pageSize, String sortField, SortOrder order, Map<String, String> filters) {
		if(results.size()<pageSize)return (cachedResults=results);
		else{
			cachedResults=new ArrayList<TestResult>(pageSize);
			for(int i=first;i<first+pageSize&&i<results.size();i++)
				cachedResults.add(results.get(i));
			return cachedResults;
		}
	}

	@Override
	public TestResult getRowData(String rowKey) {
		for (TestResult result : cachedResults){
			if (result.getId().equals(new BigInteger(rowKey))) {
				return result;
			}
 		}
		return null;
	}

	@Override
	public Object getRowKey(TestResult result) {
		return result.getId();
	}

	@Override
	public int getRowCount() {
		return results.size();
	}

	public StudentResultsDataModel getRefreshed(){
		init(test,course);
		return this;
	}
	public DateFormat getDateFormat(){
		return Formats.getDateFormat();
	}
}