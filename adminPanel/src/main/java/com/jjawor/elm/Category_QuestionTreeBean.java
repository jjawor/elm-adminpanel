package com.jjawor.elm;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;

import org.primefaces.model.TreeNode;  
import org.primefaces.model.DefaultTreeNode;  
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.jjawor.elm.authentication.AuthUtilsService;
import com.jjawor.elm.schemas.Answer;
import com.jjawor.elm.schemas.Category;
import com.jjawor.elm.schemas.Course;
import com.jjawor.elm.schemas.Question;
import com.jjawor.elm.schemas.User;
  

@Component("category_questionTreeBean")
@Scope(value="session")
public class Category_QuestionTreeBean implements Serializable{  
	private static final long serialVersionUID = 1L;
	@Autowired
	private transient CourseService courseService;   
	@Autowired
	private transient MessagesController messagesController;//for messagess
	@Autowired
	private SessionData sessionData;
	@Autowired
	private StudentsBean studentsBean;
	
	//tree data
    private TreeNode root;  
    private List<User> students;
    private List<Category> categories;
    private final String rootType="kroot";
	private final String categoryType="category";
    private final String questionType="question";
    private final String answerType="answer";
    private final String answerTrueType="answerTrue";
    private final String answerFalseType="answerFalse";
    //mini tree data
    private TreeNode categoriesRoot;  

	//for creating and selecting
	private String newCategory;
	private Question newQuestion;
	private Answer newAnswer;
    private TreeNode selectedNode;
    private int selectedNodeLevel;
    //data for web flow
    private boolean result;
    
    
    public boolean isLastSuccess(){
    	return result;
    }

    public List<User> getStudents() {
		return students;
	}

	public void setStudents(List<User> students) {
		this.students = students;
	}
    
	@PostConstruct
    public void init() {  
		//build the tree
        root = new DefaultTreeNode("Root", null); 
        students=studentsBean.getStudents();
        TreeNode main = new DefaultTreeNode(rootType,"Kategorie", root);
        main.setExpanded(true);
        categories=courseService.getCategoryQuestions(sessionData);
        if(categories==null)return;
        for(Category category:categories)
        {
        	TreeNode categoryNode = new DefaultTreeNode(categoryType,new CategoryTreeWrapper(category), main); 
        	for(Question qq:category.getQuestions()){
        		qq.setCategoryId(category.getId());//neeeded for edit
        		TreeNode questionNode=new DefaultTreeNode(questionType,new QuestionTreeWrapper(qq), categoryNode); 
        		for(Answer aa:qq.getAnswers())
        		{
        			aa.setQuestionId(qq.getId());//neeeded for edit
        			new DefaultTreeNode(aa.isIsTrue()?answerTrueType:answerFalseType,new AnswerTreeWrapper(aa), questionNode);
        		}
        	}
        		
        }
        //build mini tree
        categoriesRoot=new DefaultTreeNode("Root", null); 
        for(Category category:categories)
        {
        	new DefaultTreeNode(categoryType,new CategoryTreeWrapper(category), categoriesRoot); 
        }
    }  
	public void persistCategory(ActionEvent actionEvent){
		Category cat=new Category();
		cat.setName(getNewCategory());
		result=courseService.persistCategory(sessionData, cat)<0?false:true;
		if(result)messagesController.addOperationSuccess(actionEvent);
		else messagesController.addOperationError(actionEvent);
	}
	public void persistQuestion(ActionEvent actionEvent){
		Object src=this.getSelectedNode().getData();
		if(src instanceof CategoryTreeWrapper){
			Category cat=((CategoryTreeWrapper) src).getCategory();
			this.getNewQuestion().setCategoryId(cat.getId());
			result=courseService.persistQuestion(sessionData, this.getNewQuestion())<0?false:true;
			this.setNewQuestion(null);
		}
		else if(this.getNewQuestion().getId()!=null){
			result=courseService.persistQuestion(sessionData, this.getNewQuestion())<0?false:true;
			this.setNewQuestion(null);
		}
		else result=false;
		if(result)messagesController.addOperationSuccess(actionEvent);
		else messagesController.addOperationError(actionEvent);
	}
	public void persistAnswer(ActionEvent actionEvent){
		Object src=this.getSelectedNode().getData();
		if(src instanceof QuestionTreeWrapper){
			Question cat=((QuestionTreeWrapper) src).getQuestion();
			this.getNewAnswer().setQuestionId(cat.getId());
			result=courseService.persistAnswer(sessionData, this.getNewAnswer())<0?false:true;
			this.setNewAnswer(new Answer());
		}
		else if(this.getNewAnswer().getId()!=null){
			result=courseService.persistAnswer(sessionData, this.getNewAnswer())<0?false:true;
			this.setNewAnswer(new Answer());
		}
		else result=false;
		if(result)messagesController.addOperationSuccess(actionEvent);
		else messagesController.addOperationError(actionEvent);
	}
	public void deleteCategory(ActionEvent actionEvent){
		Object src=this.getSelectedNode().getData();
		if(src instanceof CategoryTreeWrapper){
			Category cat=((CategoryTreeWrapper) src).getCategory();
			result=courseService.deleteCategory(sessionData, cat);
		}
		else result=false;
		if(result)messagesController.addOperationSuccess(actionEvent);
		else messagesController.addOperationError(actionEvent);
	}
	public void deleteQuestion(ActionEvent actionEvent){
		Object src=this.getSelectedNode().getData();
		if(src instanceof QuestionTreeWrapper){
			Question cat=((QuestionTreeWrapper) src).getQuestion();
			result=courseService.deleteQuestion(sessionData, cat);
		}
		else result=false;
		if(result)messagesController.addOperationSuccess(actionEvent);
		else messagesController.addOperationError(actionEvent);
	}
	public void deleteAnswer(ActionEvent actionEvent){
		Object src=this.getSelectedNode().getData();
		if(src instanceof AnswerTreeWrapper){
			Answer cat=((AnswerTreeWrapper) src).getAnswer();
			result=courseService.deleteAnswer(sessionData, cat);
		}
		else result=false;
		if(result)messagesController.addOperationSuccess(actionEvent);
		else messagesController.addOperationError(actionEvent);
	}
	public void resetNewAnswer(ActionEvent actionEvent)
	{
		this.newAnswer=null;
	}
	public void resetNewQuestion(ActionEvent actionEvent)
	{
		this.newQuestion=null;
	}
	public void setSelectedAnswerAsNew(ActionEvent actionEvent)
	{
		Object data=this.getSelectedNode().getData();
		if(data instanceof AnswerTreeWrapper)
			this.newAnswer=((AnswerTreeWrapper) data).getAnswer();
	}
	public void setSelectedQuestionAsNew(ActionEvent actionEvent)
	{
		Object data=this.getSelectedNode().getData();
		if(data instanceof QuestionTreeWrapper)
			this.newQuestion=((QuestionTreeWrapper) data).getQuestion();
	}
	public void addNewAnswerToNewQuestion(ActionEvent actionEvent){
		this.getNewQuestion().getAnswers().add(new Answer());
	}
	public void removeNewAnswerFromNewQuestion(Answer answer){
		this.getNewQuestion().getAnswers().remove(answer);
	}
	public Category_QuestionTreeBean getRefreshed(){
		init();
		return this;
	}
	
    public TreeNode getRoot() {  
        return root;  
    }
    
	public TreeNode getSelectedNode() {
		return selectedNode;
	}
	public void setSelectedNode(TreeNode selectedNode) {
		/*int level=0;
		TreeNode node=selectedNode;
		while(node.getParent()!=null){node=node.getParent();level++;}
		selectedNodeLevel=level;
		System.out.println("fdsfsdf"+selectedNodeLevel);*/
		this.selectedNode = selectedNode;
	}

	public String getNewCategory() {
		return newCategory;
	}

	public void setNewCategory(String newCategory) {
		this.newCategory = newCategory;
	}

	public Question getNewQuestion() {
		if(newQuestion==null)newQuestion=new Question();
		return newQuestion;
	}

	public void setNewQuestion(Question newQuestion) {
		this.newQuestion = newQuestion;
	}

	public Answer getNewAnswer() {
		if(newAnswer==null)newAnswer=new Answer();
		return newAnswer;
	}

	public void setNewAnswer(Answer newAnswer) {
		this.newAnswer = newAnswer;
	}

	public int getSelectedNodeLevel() {
		return selectedNodeLevel;
	}

	public void setSelectedNodeLevel(int selectedNodeLevel) {
		this.selectedNodeLevel = selectedNodeLevel;
	}
	

    public String getRootType() {
		return rootType;
	}

	public String getCategoryType() {
		return categoryType;
	}

	public String getQuestionType() {
		return questionType;
	}

	public String getAnswerType() {
		return answerType;
	}
	 public String getAnswerTrueType() {
			return answerTrueType;
		}

		public String getAnswerFalseType() {
			return answerFalseType;
		}

		public List<Category> getCategories() {
			return categories;
		}

		public void setCategories(List<Category> categories) {
			this.categories = categories;
		}

		public TreeNode getCategoriesRoot() {
			return categoriesRoot;
		}

		public void setCategoriesRoot(TreeNode categoriesRoot) {
			this.categoriesRoot = categoriesRoot;
		}

	
/*	public void printSN(ActionEvent actionEvent){
		System.out.println(selectedNode);
	}*/

}
