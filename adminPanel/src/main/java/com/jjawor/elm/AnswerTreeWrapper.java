package com.jjawor.elm;

import java.io.Serializable;

import com.jjawor.elm.schemas.Answer;
import com.jjawor.elm.schemas.Category;
import com.jjawor.elm.schemas.Course;
import com.jjawor.elm.schemas.Question;
import com.jjawor.elm.schemas.User;

public class AnswerTreeWrapper implements Serializable {

	private static final long serialVersionUID = 1L;
	private Answer answer;
	public AnswerTreeWrapper(Answer answer){
		this.answer = answer;
	}
	@Override
	public String toString(){
		return answer.getText();
	}
	public Answer getAnswer() {
		return answer;
	}
	public void setAnswer(Answer answer) {
		this.answer = answer;
	}
}
