package com.jjawor.elm.ws;

import org.apache.log4j.Logger;

import com.jjawor.elm.authentication.AuthUtilsService;
import com.jjawor.elm.schemas.Response;
import com.jjawor.elm.schemas.ResultStatus;

public class WSCommons {
	public static boolean handeWSResponseErrors(Response response){
		if(response.getStatus()==ResultStatus.NO_SESSION)
			{AuthUtilsService.sessionExpired();return false;}
		if(response.getStatus()==ResultStatus.TIME_OUT)
		{return true;}//it's specific error, we need to handle it someplace else 
		else if(!response.getStatus().equals(ResultStatus.OK)){
			if(response.getMessage()!=null)
				Logger.getLogger(WSCommons.class).error(response.getMessage());
			throw new RuntimeException("Unknown WS erorr");
		}
		return true;
	}
}
