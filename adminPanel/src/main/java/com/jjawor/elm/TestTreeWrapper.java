package com.jjawor.elm;

import java.io.Serializable;

import com.jjawor.elm.schemas.Category;
import com.jjawor.elm.schemas.Course;
import com.jjawor.elm.schemas.Test;
import com.jjawor.elm.schemas.User;

public class TestTreeWrapper implements Serializable {

	private static final long serialVersionUID = 1L;
	private Test test;
	public TestTreeWrapper(Test test){
		this.test = test;
	}
	@Override
	public String toString(){
		return "Test: "+test.getName();
	}
	public Test getTest() {
		return test;
	}
	public void setTest(Test test) {
		this.test = test;
	}
}
