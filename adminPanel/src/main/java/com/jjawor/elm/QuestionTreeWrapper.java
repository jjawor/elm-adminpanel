package com.jjawor.elm;

import java.io.Serializable;

import com.jjawor.elm.schemas.Category;
import com.jjawor.elm.schemas.Course;
import com.jjawor.elm.schemas.Question;
import com.jjawor.elm.schemas.User;

public class QuestionTreeWrapper implements Serializable {

	private static final long serialVersionUID = 1L;
	private Question question;
	public QuestionTreeWrapper(Question question){
		this.question = question;
	}
	@Override
	public String toString(){
		return /*"?: "+*/question.getText();
	}
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
}
