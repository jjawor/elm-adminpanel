package com.jjawor.elm;

import java.io.Serializable;

import com.jjawor.elm.schemas.Course;
import com.jjawor.elm.schemas.User;

public class CourseTreeWrapper implements Serializable {

	private static final long serialVersionUID = 1L;
	private Course course;
	public CourseTreeWrapper(Course course){
		this.course = course;
	}
	public Course getCourse() {
		return course;
	}
	public void setCourse(Course course) {
		this.course = course;
	}
	@Override
	public String toString(){
		return course.getName();
	}
}
