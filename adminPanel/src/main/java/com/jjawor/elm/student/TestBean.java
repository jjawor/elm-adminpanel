package com.jjawor.elm.student;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.jjawor.elm.SessionData;
import com.jjawor.elm.schemas.Answer;
import com.jjawor.elm.schemas.Course;
import com.jjawor.elm.schemas.Question;
import com.jjawor.elm.schemas.SolveTestResponse;
import com.jjawor.elm.schemas.StudentAnswer;
import com.jjawor.elm.schemas.Test;
import com.jjawor.elm.services.StudentService;

@Component("testBean")
@Scope(value="session")
public class TestBean implements Serializable{
	private static final long serialVersionUID = 1L;
	@Autowired
	private transient StudentService studentService;
	@Autowired
	private SessionData sessionData;
	private Question question;
	private boolean isNextQuestion;//=true;
	private int questionNO;
	//selection
	private int selectedAnswer;
	private List<SelectItem> selectItems;
	private StudentAnswer studentAnswer;
	//data needed to communicate with ws
	private Test test;
	private Course course;
	private SolveTestResponse response;
	//time count
	private Date startDate;
	private int minLeft;
	private int secLeft;
	private boolean timeElapsed;
	
	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}
	public void getFirstQuestion(Test test,Course course){
		isNextQuestion=true;
		questionNO=1;
		question=studentService.startTest(test, course, sessionData).get(0);
		this.test=test;
		this.course=course;
		//int test time
		this.minLeft=test.getTimeForTest().intValue();
		this.secLeft=0;
		this.timeElapsed=false;
		this.startDate=new Date();
	}
	public void getNextQuestion(){
		questionNO++;
		List<Answer> ans=question.getAnswers();
		StudentAnswer studentAnswer=new StudentAnswer();
		studentAnswer.setQuestionId(question.getId());
		if(!this.getTest().isAreMultipleAnswers()){//we're getting value of radio button
			ans.get(this.getSelectedAnswer()).setIsTrue(true);
		}
		for(Answer a:ans)
			studentAnswer.getAnswers().add(a.isIsTrue());
		SolveTestResponse response=studentService.getNextQuestion(studentAnswer, test, course, sessionData);
		if(response.getQuestions().size()==0){isNextQuestion=false;this.response=response;}
		else question=response.getQuestions().get(0);
		///selectedAnswer=0;//for radio button list size chang
		selectItems=null;//for radio button list size chang
	}
	public void intitELearning(){
		getEQuestionFromResult(1);
	}
	public void getNextEQuestion(){
		getEQuestionFromResult(questionNO+1);
	}
	public void getPrevEQuestion(){
		getEQuestionFromResult(questionNO-1);
	}
	private void getEQuestionFromResult(int qn){
		questionNO=qn;
		this.question=response.getResult().getQuestions().get(qn-1);
		studentAnswer=response.getResult().getStudentAnswers().get(qn-1);
		if(!this.getTest().isAreMultipleAnswers()){//we're setting value of radio button
			int number=0;
			for(Boolean b:studentAnswer.getAnswers())
				if(b)break;else number++;
			this.setSelectedAnswer(number);
		}
	}
	public boolean isNextQuestion(){
		return isNextQuestion;
	}

	public int getQuestionNO() {
		return questionNO;
	}

	public void setQuestionNO(int questionNO) {
		this.questionNO = questionNO;
	}
	public Test getTest(){
		return test;
	}
	private static String [] colors={"#ee1111","#11ee11"};
	public String getColor(boolean c){
		return colors[c?1:0];
	}
	private static String [] abc;
	static{
		abc=new String['Z'-'A'+1];
		for(int i=0;i<abc.length;i++)
			abc[i]=""+((char)('A'+i));
	}
	public static String abc(int index){
		return abc[index];
	}

	public int getSelectedAnswer() {
		return selectedAnswer;
	}

	public void setSelectedAnswer(int selectedAnswer) {
		this.selectedAnswer = selectedAnswer;
	}

	public List<SelectItem> getSelectItems() {
		if(selectItems==null|| selectItems.isEmpty()){
			selectItems=new ArrayList<SelectItem>();
			for(int i=0;i<this.getQuestion().getAnswers().size();i++)
				selectItems.add(new SelectItem(i, "answer" + (i + 1)));
		}
		return selectItems;
	}

	public void setSelectItems(List<SelectItem> selectItems) {
		this.selectItems = selectItems;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public SolveTestResponse getResponse() {
		return response;
	}

	public void setResponse(SolveTestResponse response) {
		this.response = response;
	}

	public StudentAnswer getStudentAnswer() {
		return studentAnswer;
	}

	public void setStudentAnswer(StudentAnswer studentAnswer) {
		this.studentAnswer = studentAnswer;
	}
	
	public int getMinLeft() {
		return minLeft;
	}

	public void setMinLeft(int minLeft) {
		this.minLeft = minLeft;
	}

	public int getSecLeft() {
		return secLeft;
	}

	public void setSecLeft(int secLeft) {
		this.secLeft = secLeft;
	}

	public boolean isTimeElapsed() {
		return timeElapsed;
	}

	public void setTimeElapsed(boolean timeElapsed) {
		this.timeElapsed = timeElapsed;
	}
	
	public void countTime(){
		long timeLeft=this.getTest().getTimeForTest().longValue()*60*1000-(new Date().getTime()-startDate.getTime()+1000);
		if(timeLeft<0){
			this.minLeft=0;
			this.secLeft=0;
			timeElapsed=true;
		}
		else{
			this.minLeft=(int)(timeLeft/1000/60);
			this.secLeft=(int) (timeLeft/1000-minLeft*60);
		}
	}
}
