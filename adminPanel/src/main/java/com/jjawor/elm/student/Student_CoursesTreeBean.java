package com.jjawor.elm.student;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;

import org.primefaces.model.TreeNode;
import org.primefaces.model.DefaultTreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.jjawor.elm.CourseService;
import com.jjawor.elm.CourseTreeWrapper;
import com.jjawor.elm.MessagesController;
import com.jjawor.elm.SessionData;
import com.jjawor.elm.TestTreeWrapper;
import com.jjawor.elm.authentication.AuthUtilsService;
import com.jjawor.elm.schemas.Answer;
import com.jjawor.elm.schemas.Category;
import com.jjawor.elm.schemas.Course;
import com.jjawor.elm.schemas.Question;
import com.jjawor.elm.schemas.Test;
import com.jjawor.elm.schemas.User;
import com.jjawor.elm.services.StudentService;

@Component("student_courseTreeBean")
@Scope(value = "session")
public class Student_CoursesTreeBean implements Serializable {
	private static final long serialVersionUID = 1L;
	@Autowired
	private transient StudentService studentService;
	@Autowired
	private transient MessagesController messagesController;// for messagess
	@Autowired
	private SessionData sessionData;

	// tree data
	private TreeNode root;
	private List<Course> courses;
	private final String rootType = "kroot";
	private final String courseType = "category";
	private final String testType = "test";

	// mini tree data
	private TreeNode categoriesRoot;

	// for creating and selecting
	private TreeNode selectedNode;
	private Test selectedTest;
	private Course selectedCourse;
	// data for web flow
	private boolean result;

	public boolean isLastSuccess() {
		return result;
	}

	@PostConstruct
	public void init() {
		// build the tree
		root = new DefaultTreeNode("Root", null);
		TreeNode main = new DefaultTreeNode(rootType, "Kategorie", root);
		main.setExpanded(true);
		courses = studentService.getCourseTests(sessionData);
		if (courses == null)
			return;
		for (Course course : courses) {
			TreeNode categoryNode = new DefaultTreeNode(courseType,
					new CourseTreeWrapper(course), main);
			categoryNode.setExpanded(true);
			if(course.getTests()!=null)
			for (Test test : course.getTests()) {
				new DefaultTreeNode(testType,
						new TestTreeWrapper(test), categoryNode);
			}

		}
	}

	/*public void startTest(ActionEvent actionEvent) {
		Object data = this.getSelectedNode().getData();
		if (data instanceof QuestionTreeWrapper)
			this.newQuestion = ((QuestionTreeWrapper) data).getQuestion();
	}*/
	public void setSelectedTest(ActionEvent actionEvent) {
		Object data = this.getSelectedNode().getData();
		Object parentData=this.getSelectedNode().getParent().getData();
		if (data instanceof TestTreeWrapper && parentData instanceof CourseTreeWrapper){
			this.setSelectedTest(((TestTreeWrapper) data).getTest());
			this.setSelectedCourse(((CourseTreeWrapper) parentData).getCourse());
		}
	}

	public Student_CoursesTreeBean getRefreshed() {
		init();
		return this;
	}

	public TreeNode getRoot() {
		return root;
	}

	public TreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public String getRootType() {
		return rootType;
	}

	public TreeNode getCategoriesRoot() {
		return categoriesRoot;
	}

	public void setCategoriesRoot(TreeNode categoriesRoot) {
		this.categoriesRoot = categoriesRoot;
	}

	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}

	public String getCourseType() {
		return courseType;
	}

	public Test getSelectedTest() {
		return selectedTest;
	}

	public void setSelectedTest(Test selectedTest) {
		this.selectedTest = selectedTest;
	}

	public Course getSelectedCourse() {
		return selectedCourse;
	}

	public void setSelectedCourse(Course selectedCourse) {
		this.selectedCourse = selectedCourse;
	}
	
	public String getTestType() {
		return testType;
	}

}
