package com.jjawor.elm;

import java.io.Serializable;

import com.jjawor.elm.schemas.User;

public class UserTreeWrapper implements Serializable {

	private static final long serialVersionUID = 1L;
	private User user;
	public UserTreeWrapper(User user){
		this.user=user;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	@Override
	public String toString(){
		return user.getIndexNumber()+" "+user.getName();
	}
}
