package com.jjawor.elm.commons;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Formats {
	private static DateFormat defaultDateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static DateFormat getDateFormat() {
		return defaultDateFormat;
	}

}
