package com.jjawor.elm;

import java.io.Serializable;

import com.jjawor.elm.schemas.Category;
import com.jjawor.elm.schemas.Course;
import com.jjawor.elm.schemas.User;

public class CategoryTreeWrapper implements Serializable {

	private static final long serialVersionUID = 1L;
	private Category category;
	public CategoryTreeWrapper(Category category){
		this.category = category;
	}
	@Override
	public String toString(){
		return "Kategoria: "+category.getName();
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
}
