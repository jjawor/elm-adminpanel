/**
 *
 */
package com.jjawor.elm;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.jjawor.elm.schemas.GetUserListRequest;
import com.jjawor.elm.schemas.GetUserListResponse;
import com.jjawor.elm.schemas.User;
@Service("usersModel")
@Scope(value = "session", proxyMode = org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS) 
public class UserDataModel extends LazyDataModel<User> {
	private static final long serialVersionUID = -8832831134966938627L;
	@Autowired
	private transient UserService userService;
	@Autowired
	private SessionData sessionData;
	private List<User> users;
	private User selectedUser;
	//
	private List<User> caschedUsers;
	@PostConstruct
    public void init(){
		 users=userService.getUsers(sessionData);
	 }

	@Override
	public List<User> load(int first, int pageSize, String sortField, SortOrder order, Map<String, String> filters) {
		//users=userService.getUsers(sessionData);
		if(users.size()<pageSize)return (caschedUsers=users);
		else{
			caschedUsers=new ArrayList<User>(pageSize);
			for(int i=first;i<first+pageSize&&i<users.size();i++)
				caschedUsers.add(users.get(i));
			return caschedUsers;
		}
	}

	@Override
	public User getRowData(String rowKey) {
		for (User user : caschedUsers){
			if (user.getId().equals(new BigInteger(rowKey))) {
				return user;
			}
 		}
		return null;
	}

	@Override
	public Object getRowKey(User user) {
		return user.getId();
	}

	@Override
	public int getRowCount() {
		return users.size();
	}

	public UserDataModel getRefreshed(){
		users=userService.getUsers(sessionData);
		return this;
	}
	
	public User getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(User selectedUser) {
		this.selectedUser = selectedUser;
	}
	
	public void onRowSelect(SelectEvent event) {
      //yes yes do nothing
    }
	public boolean persist(User user){
		return (userService.persistUser(sessionData, user)<0?false:true);
	}
	public boolean remove(User user){
		return userService.deleteUser(sessionData, user);
	}
	

}