package com.jjawor.elm;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.jjawor.elm.schemas.User;


@Component("studentsBean")
@Scope(value="session")
public class StudentsBean implements Serializable{
	private static final long serialVersionUID = 1L;
	@Autowired
	private transient UserService userService;  
	@Autowired
	private SessionData sessionData;
	private List<User> students;
	@PostConstruct
    public void init() {  
		students=userService.getStudents(sessionData);
	}
	public List<User> getStudents() {
		return students;
	}
	public void setStudents(List<User> students) {
		this.students = students;
	}
	public User getStudentById(BigInteger id) {
		User user=new User();
		user.setId(id);
		User realUser=students.get(Collections.binarySearch(students, user, new UserIdComparator()));
		return realUser;
	}
}
