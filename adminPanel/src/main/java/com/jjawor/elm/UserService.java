package com.jjawor.elm;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.jjawor.elm.schemas.CreateUserRequest;
import com.jjawor.elm.schemas.CreateUserResponse;
import com.jjawor.elm.schemas.GetUserListRequest;
import com.jjawor.elm.schemas.GetUserListResponse;
import com.jjawor.elm.schemas.RequestType;
import com.jjawor.elm.schemas.ResultStatus;
import com.jjawor.elm.schemas.User;
import com.jjawor.elm.ws.UserType;
import com.jjawor.elm.ws.WSCommons;

@Service("userService")
public class UserService{
	@Autowired
	private  WebServiceTemplate getUserListWS;
	@Autowired
	private  WebServiceTemplate createUserWS;
	@SuppressWarnings("deprecation")
	@Autowired
	private PasswordEncoder passwordEncoder;
		public List<User> getUsers(SessionData sessionData){
			 Logger.getLogger(this.getClass()).info("UserService get users");
			 GetUserListRequest request=new GetUserListRequest();
			 request.setToken(sessionData.getWSToken());
			 GetUserListResponse res=(GetUserListResponse) getUserListWS.marshalSendAndReceive(request);
			 Logger.getLogger(this.getClass()).info("getUsers status:"+res.getStatus());
			 if(!WSCommons.handeWSResponseErrors(res))return null;
			 return res.getUsers();
		}
		public List<User> getStudents(SessionData sessionData){
			 Logger.getLogger(this.getClass()).info("UserService get students");
			 GetUserListRequest request=new GetUserListRequest();
			 request.setToken(sessionData.getWSToken());
			 GetUserListResponse res=(GetUserListResponse) getUserListWS.marshalSendAndReceive(request);
			 if(!WSCommons.handeWSResponseErrors(res))return null;
			 Iterator<User> it=res.getUsers().iterator();
			 while(it.hasNext()){
				 if(!it.next().getType().equals(UserType.STUDENT.name()))
				 	it.remove();
			 }
			 return res.getUsers();
		}
		public long persistUser(SessionData sessionData,User user){
			CreateUserRequest request=new CreateUserRequest();
			request.setToken(sessionData.getWSToken());
			if(user.getPassword()!=null)user.setPassword(encodePassword(user.getPassword(),user.getLogin()));
			request.setUser(user);
			if(user.getId()==null){
				request.setOperationType(RequestType.CREATE);
			}
			else
			{
				request.setOperationType(RequestType.MODIFY);
			}
			CreateUserResponse res=(CreateUserResponse) createUserWS.marshalSendAndReceive(request);
			Logger.getLogger(this.getClass()).info("persistUser status: "+res.getStatus());
			if(!WSCommons.handeWSResponseErrors(res))return -1l;
			return res.getId().longValue();
		}

		@SuppressWarnings("deprecation")
		private String encodePassword(String password,String salt){
			if(passwordEncoder==null)return password;
			return passwordEncoder.encodePassword(password,salt);
		}
		public boolean deleteUser(SessionData sessionData,User user){
			CreateUserRequest request=new CreateUserRequest();
			request.setToken(sessionData.getWSToken());
			request.setUser(user);
			request.setOperationType(RequestType.DELETE);
			CreateUserResponse res=(CreateUserResponse) createUserWS.marshalSendAndReceive(request);
			Logger.getLogger(this.getClass()).info("deleteUser status: "+res.getStatus());
			if(!WSCommons.handeWSResponseErrors(res))return false;
			return true;
		}
}
