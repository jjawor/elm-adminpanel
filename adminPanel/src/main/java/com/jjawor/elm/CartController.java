package com.jjawor.elm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.jjawor.elm.schemas.SignInRequest;

@Controller
public class CartController {

	@Autowired
	private WebServiceTemplate webServiceTemplate;
	
    @RequestMapping("/home.do")
    public void doHome() {
    	System.out.println('a');
    	System.out.println(webServiceTemplate.getMarshaller());
    	System.out.println(webServiceTemplate.getUnmarshaller());
    	System.out.println(webServiceTemplate);
    	SignInRequest s=new SignInRequest();
    	s.setLogin("admin");
    	s.setPassword("admin");
    	System.out.println(webServiceTemplate.marshalSendAndReceive(s));
    }
}
