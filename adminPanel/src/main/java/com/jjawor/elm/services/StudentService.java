package com.jjawor.elm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.jjawor.elm.SessionData;
import com.jjawor.elm.schemas.Course;
import com.jjawor.elm.schemas.GetCourseTestListRequest;
import com.jjawor.elm.schemas.GetCourseTestListResponse;
import com.jjawor.elm.schemas.Question;
import com.jjawor.elm.schemas.SolveTestRequest;
import com.jjawor.elm.schemas.SolveTestResponse;
import com.jjawor.elm.schemas.StudentAnswer;
import com.jjawor.elm.schemas.Test;
import com.jjawor.elm.ws.WSCommons;

@Service("studentService")
public class StudentService{
	@Autowired
	private  WebServiceTemplate elmWS;
		public List<Course> getCourseTests(SessionData sessionData){
		 GetCourseTestListRequest request=new GetCourseTestListRequest();
		 request.setToken(sessionData.getWSToken());
		 GetCourseTestListResponse res=(GetCourseTestListResponse) elmWS.marshalSendAndReceive(request);
		 if(!WSCommons.handeWSResponseErrors(res))return null;
		 return res.getCourses();
	}
		public List<Question> startTest(Test test,Course course,SessionData sessionData){
			SolveTestRequest request=new SolveTestRequest();
			 request.setToken(sessionData.getWSToken());
			 request.setCourseId(course.getId());
			 request.setTestId(test.getId());
			 SolveTestResponse res=(SolveTestResponse) elmWS.marshalSendAndReceive(request);
			 if(!WSCommons.handeWSResponseErrors(res))return null;
			 return res.getQuestions();
		}
		public SolveTestResponse getNextQuestion(StudentAnswer answer,Test test,Course course,SessionData sessionData){
			SolveTestRequest request=new SolveTestRequest();
			 request.setToken(sessionData.getWSToken());
			 request.setCourseId(course.getId());
			 request.setTestId(test.getId());
			 request.getStudentAnswers().add(answer);
			 SolveTestResponse res=(SolveTestResponse) elmWS.marshalSendAndReceive(request);
			 if(!WSCommons.handeWSResponseErrors(res))return null;
			 return res;
		}
}