package com.jjawor.elm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.jjawor.elm.schemas.SignInRequest;

//@Controller
public class AdminHomeController {


	@Autowired
	private SessionData sessionData;
	@Autowired
	private UserService userService;
    @RequestMapping("/admin.do")
    public void doHome() {
    	//System.out.println(userService.getUsers());
    	//System.out.println(sessionData);
    }
}
