package com.jjawor.elm;

import javax.faces.event.ActionEvent;  
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;  

import org.springframework.stereotype.Controller;

  
@Controller("messagesController")
public class MessagesController {  
  
	public void addOperationError(ActionEvent actionEvent) {  
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Operacja nie powiodła się", null));  
     }
	public void clearError(ActionEvent actionEvent) {  
        FacesContext.getCurrentInstance().getMessages().remove();
     }
	public void addOperationSuccess(ActionEvent actionEvent) { 
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Operacja powiodła się", null));  
     }
}  
  
