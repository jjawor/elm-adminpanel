package com.jjawor.elm.authentication;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.ws.client.WebServiceTransportException;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.jjawor.elm.SessionData;
import com.jjawor.elm.schemas.ResultStatus;
import com.jjawor.elm.schemas.SignInRequest;
import com.jjawor.elm.schemas.SignInResponse;
import com.jjawor.elm.ws.UserType;
//import org.springframework.security.crypto.password.PasswordEncoder;
@SuppressWarnings("deprecation")
//@Component
public class WSAuthenticatonProvider implements AuthenticationProvider {
	@Autowired
	private WebServiceTemplate signInWS;
	@Autowired
	private SessionData sessionData;
	private PasswordEncoder passwordEncoder;
	@Override
	public Authentication authenticate(Authentication a)
			throws AuthenticationException {
		List<GrantedAuthority> grantedAuths = new ArrayList<GrantedAuthority>();
        String login=a.getName();
        String password=encodePassword(a.getCredentials().toString(),a.getName());
        SignInRequest authRequest=new SignInRequest();
        authRequest.setLogin(login);
        authRequest.setPassword(password);
        SignInResponse res=null;
        try{
        	res=(SignInResponse) signInWS.marshalSendAndReceive(authRequest);
        }
        catch(WebServiceTransportException e){
        	e.printStackTrace();
        	throw new AuthenticationServiceException("ws transoprt error");
        }
        catch(RuntimeException e){
        	e.printStackTrace();
        	throw new AuthenticationServiceException("ws unknown error");
        }
        Logger.getLogger(this.getClass()).info("Attempting to sign in: "+res.getUserType()+" ;id: "+res.getId()+" ;status: "+res.getStatus());
        if(!res.getStatus().equals(ResultStatus.OK))throw new AuthenticationCredentialsNotFoundException("Wrong login or password");
        //if(!res.getUserType().equals(UserType.ADMIN.name())&&!res.getUserType().equals(UserType.PROFESSOR.name()))throw new InsufficientAuthenticationException("Insufficient access level");
        sessionData.setWSToken(res.getToken());
        sessionData.setUserId(res.getId());
        grantedAuths.add(new SimpleGrantedAuthority("ROLE_"+res.getUserType()));
        Authentication auth = new UsernamePasswordAuthenticationToken(login, password, grantedAuths);
        return auth;
	}
	private String encodePassword(String password,String salt){
		if(passwordEncoder==null)return password;
		return passwordEncoder.encodePassword(password,salt);
	}
	@Override
	public boolean supports(Class<?> auth) {
		return auth.equals(UsernamePasswordAuthenticationToken.class);
	}

	public PasswordEncoder getPasswordEncoder() {
		return passwordEncoder;
	}

	public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}
}
