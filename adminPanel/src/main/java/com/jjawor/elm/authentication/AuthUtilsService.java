package com.jjawor.elm.authentication;

import java.io.IOException;

import javax.faces.context.FacesContext;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

//@Service("authUtilsService")
public class AuthUtilsService {
	public static void logOut(){
		SecurityContextHolder.clearContext();
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void sessionExpired(){
		SecurityContextHolder.clearContext();
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("/j_spring_security_logout");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
