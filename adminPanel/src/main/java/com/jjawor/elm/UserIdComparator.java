package com.jjawor.elm;

import java.util.Comparator;

import com.jjawor.elm.schemas.User;

public class UserIdComparator implements Comparator<User> {
	@Override
	public int compare(User o1, User o2) {
		return o1.getId().compareTo(o2.getId());
	}

}
